# budgie-screenshot-applet

A Budgie applet for taking and uploading screenshots to Imgur and Imagebin.

https://github.com/cybre/budgie-screenshot-applet

How to clone this repo:

```
git clone https://gitlab.com/reborn-os-team/rebornos-packages/budgie-screenshot-applet.git
```

